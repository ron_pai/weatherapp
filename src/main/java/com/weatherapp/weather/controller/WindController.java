package com.weatherapp.weather.controller;

import com.weatherapp.weather.*;
import com.weatherapp.weather.business.businesslogic.*;
import com.weatherapp.weather.models.*;

import org.springframework.web.bind.annotation.*;

@RestController
public class WindController {

	/**
	 * the cache
	 */
	private WindDataCache cache;
	
	/**
	 * The business logic to retrieve the wind data
	 */
	private IWindBL windBL;
	
	
	public WindController() {
		cache = WindDataCache.getCache(); 
		windBL = new WindBL();
	}
	
	public WindController(IWindBL bl) {
		this.windBL = bl;
		this.cache = null;
	}
	
	/**
	 * Retrieve the wind data
	 * @param zipCode the zipcode of the wind data
	 * @return the wind data
	 */
	 @RequestMapping(value = "/api/v1/wind/{zipCode}", method = RequestMethod.GET)
	 public WindData getWind(@PathVariable("zipCode")int zipCode) {
		 WindData data = null;
		 if(cache != null) {
			 data = cache.getCacheObject(zipCode);
		 }
		 
		 if (data == null) {
			 data =  windBL.getWindData(zipCode);
			 
			 if(data != null && cache != null) {
				 cache.addCacheObject(zipCode, data);
			 }
		 }
		 
		 return data;
	 }
}
