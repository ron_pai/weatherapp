package com.weatherapp.weather.business.businesslogic;

public interface IHttpGet {
	String httpGet(String url);
}
