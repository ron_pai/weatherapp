package com.weatherapp.weather.business.businesslogic;

import com.weatherapp.weather.models.WindData;

public interface IWindBL {
	 WindData getWindData(int zipCode) ;
}
