package com.weatherapp.weather.business.businesslogic;

import java.io.*;
import java.net.*;

import org.json.JSONObject;
import com.weatherapp.weather.models.*;

public class WindBL implements IWindBL {
	/**
	 * String representation of the application id to use the weather application
	 */
	public static String APP_ID = "c52d8e7d4209b6696709810e1391e160";
	public static String URL_FORMAT = "http://api.openweathermap.org/data/2.5/weather?zip=%d,us&appid=%s";
	private IHttpGet httpGet;
	
	/**
	 * Initilization of WindBL
	 */
	public WindBL() {
		httpGet = new HttpGet();
	}
	
	/**
	 * Initialization WindBL with a given http getter
	 * @param http
	 */
	public WindBL(IHttpGet http) {
		this.httpGet = http;
	}
	
	/***
	 * Calls the openweathermap.org api to get the wind speed and direction
	 * @param zipCode the zipcode
	 * @return the wind data of the zipcode
	 */
	public WindData getWindData(int zipCode) {
		try {
			
			String fullUrl = String.format(URL_FORMAT, zipCode, APP_ID);
			String json = httpGet.httpGet(fullUrl);
			
			if (json != null) {
				JSONObject obj = new JSONObject(json.toString());
				JSONObject windObject = obj.getJSONObject("wind");
				
				return new WindData(windObject.getDouble("speed"), windObject.getDouble("deg"));
			}
		
		} catch (RuntimeException e) {
			e.printStackTrace();
		} catch (Exception e) {
				e.printStackTrace();
		}
		
		return null;
	}
}
