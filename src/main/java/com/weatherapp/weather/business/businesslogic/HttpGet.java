package com.weatherapp.weather.business.businesslogic;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class HttpGet implements IHttpGet {

	/**
	 * An http GET call to the string url
	 */
	@Override
	public String httpGet(String strUrl) {
		try {
			URL url= new URL(strUrl);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");
	
			System.out.println("Attempting to retrieve information from : " + strUrl) ;
			
			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ conn.getResponseCode());
			}
	
			BufferedReader br = new BufferedReader(new InputStreamReader(
				(conn.getInputStream())));
	
			StringBuilder json = new StringBuilder();
			String output;
			System.out.println("Output from Server ....");
			while ((output = br.readLine()) != null) {
				json.append(output);
			}
			
			System.out.println(json.toString());
			
			conn.disconnect();
			
			return json.toString();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
}
