package com.weatherapp.weather.business.businesslogic;

import java.util.*;

import com.weatherapp.weather.models.WindData;

/**
 * Caches an object into a list and delete them after 15 minutes
 * @author Ron
 *
 */
public class WindDataCache extends Thread {
	
	private static WindDataCache CACHE = new WindDataCache();
	
	private Map<Integer, CacheObject> cacheMap; 
	private boolean isDispose = false;
	private Object lock = new Object();
	
	private WindDataCache(){
		cacheMap = new HashMap<Integer, CacheObject>();
		this.start();
	}
	
	public void setDone() {
		isDispose = true;
	}
	
	public static WindDataCache getCache() {
		return WindDataCache.CACHE;
	}
	
	/**
	 * Continuously run to remove cache from the map
	 */
	public void run() {
		while(!isDispose) {
			synchronized (lock) {
				cacheMap.entrySet().removeIf(e -> {
					return e.getValue().getExpirationTime() < System.currentTimeMillis();
				});
			}
			
			try {
				Thread.sleep(1000);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Retrieve the cached item
	 * @param zipCode
	 * @return
	 */
	public WindData getCacheObject(int zipCode) {
		synchronized (lock) {
			if (cacheMap.containsKey(zipCode)) {
				return cacheMap.get(zipCode).getObject();
			}
		}
		
		return null;
	}
	
	/**
	 * Insert an item into the cache
	 * @param zipCode
	 * @param object
	 */
	public void addCacheObject(int zipCode, WindData object) {
		synchronized (lock) {
			if (!cacheMap.containsKey(zipCode)) {
				System.out.println("Inserting wind data into cache for zipcode " + zipCode);
				cacheMap.put(zipCode, new CacheObject(object, System.currentTimeMillis() + (1000 * 60 * 15)));
			}
		}
	}
	
	public void clearCache() {
		synchronized (lock) {
			System.out.println("Clearing cache...");
			if (cacheMap != null) {
				cacheMap.clear();
			}
		}
	}
	
	/**
	 * Cache object that contains the object to cache and the time limit of cache
	 * @author Ron
	 *
	 */
	public class CacheObject {
		/**
		 * object to cache
		 */
		private WindData object;
		
		/**
		 * expiration time
		 */
		private long expirationTime;
		
		public CacheObject(WindData object, long expirationTime) {
			this.object = object;
			this.expirationTime = expirationTime;
		}
		
		public long getExpirationTime() {
			return this.expirationTime;
		}
		
		public WindData getObject() {
			return this.object;
		}
	}
}
