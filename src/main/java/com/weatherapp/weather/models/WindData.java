package com.weatherapp.weather.models;

import com.fasterxml.jackson.annotation.JsonProperty;

public class WindData extends Object {
	
	public WindData() {
	   this.windSpeed = 0;
	   this.windDirection = 0;
   }
	
	public WindData(double speed, double direction) {
		super();
		this.windSpeed = speed;
		this.windDirection = direction;
	}
	
   private double windSpeed;
   private double windDirection;

   @JsonProperty("Wind Speed")
   public double getWindSpeed() {
	   return this.windSpeed;
   }
   
   @JsonProperty("Wind Direction")
   public double getWindDirection() {
	   return this.windDirection;
   }
   
   //getters and setters
 
   @Override
   public String toString() {
      return "Employee [wind speed=" + this.windSpeed + "windDirection=" + this.windDirection + "]";
   }
}
