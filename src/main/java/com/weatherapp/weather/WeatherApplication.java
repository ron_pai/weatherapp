package com.weatherapp.weather;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.weatherapp.weather.business.businesslogic.WindDataCache;

@SpringBootApplication
public class WeatherApplication {

	public static void main(String[] args) {
		SpringApplication.run(WeatherApplication.class, args);
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String input;
		try {
			while((input = br.readLine()) != null) {
				if (input.equalsIgnoreCase("clear")) {
					WindDataCache.getCache().clearCache();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
