package com.weatherapp.weather;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Test;

import com.weatherapp.weather.business.businesslogic.IHttpGet;
import com.weatherapp.weather.business.businesslogic.WindBL;
import com.weatherapp.weather.models.WindData;

public class WindBLTest {

	/**
	 * Validity test
	 */
	@Test
	public void WindBL_GetWindDataValidTest() {
		//Create return results
		int zipCode = 89113;
		double expectedWindSpeed = 7.7;
		double expectedWindDirection = 250;
		String expectedURL = String.format(WindBL.URL_FORMAT, zipCode, WindBL.APP_ID);
		String expectedJson = String.format("{\"wind\":{\"speed\":%f,\"deg\":%f,\"gust\":13.4}}", expectedWindSpeed, expectedWindDirection);
		
		final IHttpGet mockHttp = mock(IHttpGet.class);
		when(mockHttp.httpGet(expectedURL)).thenReturn(expectedJson);
		WindBL windBL = new WindBL(mockHttp);
		WindData data = windBL.getWindData(zipCode);
		
		assertEquals(data.getWindSpeed(), expectedWindSpeed, 0.01);
		assertEquals(data.getWindDirection(), expectedWindDirection, 0.01);
	}
	
	/**
	 * Null return, invalid test
	 */
	@Test
	public void WindBL_GetWindDataBadTest() {
		int zipCode = 89113;
		String expectedURL = String.format(WindBL.URL_FORMAT, zipCode, WindBL.APP_ID);
		
		final IHttpGet mockHttp = mock(IHttpGet.class);
		when(mockHttp.httpGet(expectedURL)).thenReturn(null);
		WindBL windBL = new WindBL(mockHttp);
		WindData data = windBL.getWindData(zipCode);
		
		assertNull(data);
	}
}
