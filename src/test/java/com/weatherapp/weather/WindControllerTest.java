package com.weatherapp.weather;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

import org.junit.Test;

import com.weatherapp.weather.business.businesslogic.*;
import com.weatherapp.weather.controller.WindController;
import com.weatherapp.weather.models.WindData;

public class WindControllerTest {
	
	@Test
	public void WindController_GetWindValidTest() {
		
		//Create return results
		int zipCode = 89113;
		WindData expected = new WindData(15, 18.0);
		
		final IWindBL mockWindBL = mock(IWindBL.class);
		when(mockWindBL.getWindData(zipCode)).thenReturn(expected);
		
		WindController controller = new WindController(mockWindBL);
		WindData actual = controller.getWind(zipCode);
		
		assertEquals(expected, actual);
	}
}
