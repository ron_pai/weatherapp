package com.weatherapp.weather;

import org.junit.Test;
import com.weatherapp.weather.models.WindData;
import static org.junit.Assert.*;

public class WindDataTest {
	@Test
	public void InitializationTest() { 
		// test 1
		WindData data = new WindData(1, 1);
		assertEquals(data.getWindSpeed(), 1, 0.01);
		assertEquals(data.getWindDirection(), 1, 0.01);
		
		
		// test 2
		WindData data2 = new WindData(10, 30);
		assertEquals(data2.getWindSpeed(), 10, 0.01);
		assertEquals(data2.getWindDirection(), 30, 0.01);
		
	}
}
